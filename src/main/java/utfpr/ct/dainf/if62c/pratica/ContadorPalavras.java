/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author zacaron
 */
public class ContadorPalavras {
    private BufferedReader reader;
    
    public ContadorPalavras(String path) {
        try {
            this.reader = new BufferedReader(new FileReader(path));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ContadorPalavras.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Map getPalavras() {
        String linha = null;
        Map<String,Integer> mapPalavras = new LinkedHashMap<String,Integer>();
        Scanner scanner = new Scanner(reader).useDelimiter("\\ |\\n");
        
        try {
            while(scanner.hasNext()) {
                String token = scanner.next();
                Integer cont = mapPalavras.get(token);
                if (cont != null) {
                    mapPalavras.put(token, cont+1);
                } else {
                    mapPalavras.put(token, 1);
                }
            }
            
            scanner.close();
            reader.close();
        } catch (IOException ex) {
            Logger.getLogger(ContadorPalavras.class.getName()).log(Level.SEVERE, null, ex);
        }           
        return mapPalavras;
    }
    
    /**
     * @return the reader
     */
    public BufferedReader getReader() {
        return reader;
    }

    /**
     * @param reader the reader to set
     */
    public void setReader(BufferedReader reader) {
        this.reader = reader;
    }
    
}
